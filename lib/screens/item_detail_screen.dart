import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:the_yummy/pages/item_detail_page.dart';
import 'package:the_yummy/widgets/item_detail_navigation.dart';

class ItemDetailScreen extends StatelessWidget {
  const ItemDetailScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ItemDetailPage(),
      bottomNavigationBar: ItemDetailNavigation(),
    );
  }

}
