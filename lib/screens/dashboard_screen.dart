import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:the_yummy/pages/dashboard_page.dart';
import 'package:the_yummy/widgets/dashboard_bottom_navigation.dart';

class DashboardScreen extends StatelessWidget {
  const DashboardScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: DashboardPage(),
      bottomNavigationBar: DashboardBottomNavigation(),
    );
  }

}
