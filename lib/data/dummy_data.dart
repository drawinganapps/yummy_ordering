import 'package:the_yummy/helper/color_helper.dart';
import 'package:the_yummy/models/category_model.dart';
import 'package:the_yummy/models/item_model.dart';

class DummyData {
  static List<CategoryModel> categories = [
    CategoryModel(1, 'Popular'),
    CategoryModel(2, 'Fruits'),
    CategoryModel(3, 'Drinks'),
    CategoryModel(4, 'Pizza'),
  ];

  static List<ItemModel> favoriteFruits = [
    ItemModel(1, 'Tomato 1.', 'tomato.png', 4.2, ColorHelper.lightPink),
    ItemModel(2, 'Lime 2.', 'lime.png', 4.2, ColorHelper.lightYellow),
  ];

  static List<ItemModel> favoriteDrinks = [
    ItemModel(
        3, 'Local Performed Wine', 'wine.png', 25.8, ColorHelper.lightPink),
    ItemModel(
        4, 'Orange Juice in Jug', 'orange.png', 5.2, ColorHelper.lightYellow),
  ];

  static String copyText =
      'Orange JuiceL Health Benefits, Nutritional Value & Calories. Orange juice, as you can probably guess, is the juice squeezed from the popular and delicious citrus fruit, oranges.';
}
