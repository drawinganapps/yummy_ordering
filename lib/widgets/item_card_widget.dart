import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:the_yummy/helper/color_helper.dart';
import 'package:the_yummy/models/item_model.dart';

class ItemCardWidget extends StatelessWidget {
  final ItemModel item;

  const ItemCardWidget({super.key, required this.item});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: Get.width * 0.05, right: Get.width * 0.05),
      height: Get.height * 0.2,
      child: Stack(
        children: [
          Container(
            height: Get.height * 0.2,
            width: Get.width,
            padding: EdgeInsets.all(Get.width * 0.03),
            margin: EdgeInsets.only(top: Get.height * 0.05),
            decoration: BoxDecoration(
              color: ColorHelper.white,
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                  color: ColorHelper.dark.withOpacity(0.1),
                  spreadRadius: 1,
                  blurRadius: 2,
                  offset: const Offset(0, 2),
                ),
              ],
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  width: Get.width * 0.4,
                  child: Text(item.name,
                      style: const TextStyle(
                          fontSize: 20, fontWeight: FontWeight.w600)),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text('Price', style: TextStyle(
                      fontWeight: FontWeight.w600
                    )),
                    Text('\$${item.price.toStringAsFixed(2)}',
                        style: TextStyle(
                            fontSize: 20,
                            color: ColorHelper.primary,
                            fontWeight: FontWeight.w600)),
                  ],
                )
              ],
            ),
          ),
          Positioned(
              right: 0,
              top: 0,
              child: Image.asset(
                'assets/images/${item.icon}',
                height: Get.height * 0.2,
                fit: BoxFit.cover,
              ))
        ],
      ),
    );
  }
}
