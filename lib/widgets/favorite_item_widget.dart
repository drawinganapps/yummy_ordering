import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:the_yummy/helper/color_helper.dart';
import 'package:the_yummy/models/item_model.dart';

class FavoriteItemWidget extends StatelessWidget {
  final ItemModel item;
  const FavoriteItemWidget({super.key, required this.item});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width * 0.42,
      decoration: BoxDecoration(
        color: ColorHelper.white,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(
            color: ColorHelper.dark.withOpacity(0.1),
            spreadRadius: 1,
            blurRadius: 2,
            offset: Offset(0, 2),
          ),
        ],
      ),
      padding: EdgeInsets.only(left: Get.width * 0.03, right: Get.width * 0.03),
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.only(right: Get.width * 0.02),
            decoration: BoxDecoration(
              color: item.bgColor,
              borderRadius: BorderRadius.circular(5)
            ),
            padding: const EdgeInsets.all(5),
            child: Image.asset('assets/images/${item.icon}', height: Get.height * 0.03,),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(item.name, style: const TextStyle(
                fontSize: 18
              )),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Icon(Icons.star, size: 10,  color: ColorHelper.primary),
                  Icon(Icons.star, size: 10,  color: ColorHelper.primary),
                  Icon(Icons.star, size: 10,  color: ColorHelper.primary),
                  Icon(Icons.star, size: 10,  color: ColorHelper.primary),
                  Icon(Icons.star, size: 10,  color: ColorHelper.primary),
                ],
              )
            ],
          )
        ],
      ),
    );
  }

}
