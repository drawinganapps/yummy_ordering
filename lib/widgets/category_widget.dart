import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:the_yummy/helper/color_helper.dart';
import 'package:the_yummy/models/category_model.dart';

class CategoryWidget extends StatelessWidget {
  final CategoryModel category;
  final bool isSelected;

  const CategoryWidget({super.key, required this.category, required this.isSelected});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: Get.width * 0.05, right: Get.width * 0.05),
      decoration: BoxDecoration(
        color: isSelected ? ColorHelper.primary : ColorHelper.secondary,
        borderRadius: BorderRadius.circular(10)
      ),
      child: Center(
        child: Text(category.name, style: TextStyle(
            color: isSelected ? ColorHelper.white : ColorHelper.darkGrey,
            fontWeight: FontWeight.w500,
          fontSize: 17
        )),
      ),
    );
  }

}
