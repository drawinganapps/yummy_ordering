import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:the_yummy/helper/color_helper.dart';

class ItemDetailNavigation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: Get.height * 0.12,
      padding: EdgeInsets.all(Get.width * 0.03),
      child: Container(
        padding:
            EdgeInsets.only(left: Get.width * 0.03, right: Get.width * 0.03),
        decoration: BoxDecoration(
            color: ColorHelper.primary,
            borderRadius: BorderRadius.circular(25)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Container(
                  margin: EdgeInsets.only(right: Get.width * 0.015),
                  child: Text('Total',
                      style: TextStyle(
                          color: ColorHelper.white,
                          fontSize: 18,
                          fontWeight: FontWeight.w600)),
                ),
                Text('\$26.00',
                    style: TextStyle(
                        color: ColorHelper.white,
                        fontSize: 25,
                        fontWeight: FontWeight.w600))
              ],
            ),
            Container(
              padding: EdgeInsets.only(
                  top: Get.width * 0.04,
                  bottom: Get.width * 0.04,
                  left: Get.width * 0.1,
                  right: Get.width * 0.1),
              decoration: BoxDecoration(
                  color: ColorHelper.white,
                  borderRadius: BorderRadius.circular(10)),
              child: const Text('Buy Now',
                  style: TextStyle(fontSize: 24, fontWeight: FontWeight.w600)),
            )
          ],
        ),
      ),
    );
  }
}
