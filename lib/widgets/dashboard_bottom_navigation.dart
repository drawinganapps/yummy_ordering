import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:the_yummy/helper/color_helper.dart';

class DashboardBottomNavigation extends StatelessWidget {
  const DashboardBottomNavigation({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: Get.height * 0.12,
      padding: EdgeInsets.all(Get.width * 0.03),
      child: Container(
        decoration: BoxDecoration(
            color: ColorHelper.primary,
            borderRadius: BorderRadius.circular(25)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              decoration: BoxDecoration(color: ColorHelper.primary, shape: BoxShape.circle),
              child: const Icon(Icons.search_rounded, size: 30),
            ),
            Container(
              decoration: BoxDecoration(color: ColorHelper.primary, shape: BoxShape.circle),
              child: const Icon(Icons.settings_outlined, size: 30),
            ),
            Container(
              decoration: BoxDecoration(color: ColorHelper.primary, shape: BoxShape.circle),
              child: Container(
                width: Get.width * 0.16,
                height: Get.width * 0.16,
                decoration: BoxDecoration(
                  color: ColorHelper.dark,
                  shape: BoxShape.circle
                ),
                child: Icon(Icons.home_rounded, size: 30, color: ColorHelper.white),
              ),
            ),
            Container(
              decoration: BoxDecoration(color: ColorHelper.primary, shape: BoxShape.circle),
              child: const Icon(Icons.notifications_outlined, size: 30),
            ),
            Container(
              decoration: BoxDecoration(color: ColorHelper.primary, shape: BoxShape.circle),
              child: const Icon(Icons.account_circle_outlined, size: 30),
            ),
          ],
        ),
      ),
    );
  }
}
