import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:the_yummy/helper/color_helper.dart';

class ItemCounterWidget extends StatelessWidget {
  const ItemCounterWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: Get.height * 0.06,
      width: Get.width * 0.3,
      padding: EdgeInsets.only(left: Get.width * 0.03, right: Get.width * 0.03),
      decoration: BoxDecoration(
        color: ColorHelper.red,
        borderRadius: BorderRadius.circular(15)
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Icon(Icons.remove_rounded, color: ColorHelper.white),
          Text('5', style: TextStyle(
            color: ColorHelper.white,
            fontWeight: FontWeight.w600,
            fontSize: 22
          )),
          Icon(Icons.add_rounded, color: ColorHelper.white),
        ],
      ),
    );
  }

}
