import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:the_yummy/controller/item_controller.dart';
import 'package:the_yummy/data/dummy_data.dart';
import 'package:the_yummy/models/category_model.dart';
import 'package:the_yummy/models/item_model.dart';
import 'package:the_yummy/routes/AppRoutes.dart';
import 'package:the_yummy/widgets/category_widget.dart';
import 'package:the_yummy/widgets/favorite_item_widget.dart';
import 'package:the_yummy/widgets/item_card_widget.dart';

class DashboardPage extends StatelessWidget {
  const DashboardPage({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ItemController>(builder: (controller) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(
                top: Get.height * 0.035),
            padding: EdgeInsets.only(
                left: Get.width * 0.05, right: Get.width * 0.05),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                    decoration: const BoxDecoration(),
                    child: Image.asset('assets/logos/logo_icon.png',
                        width: Get.width * 0.05)),
                Container(
                    decoration: const BoxDecoration(),
                    child: Image.asset('assets/logos/logo_text.png',
                        width: Get.width * 0.3, fit: BoxFit.cover)),
                Container(
                    decoration: const BoxDecoration(shape: BoxShape.circle),
                    child: Image.asset('assets/logos/man.png',
                        width: Get.width * 0.1, fit: BoxFit.cover)),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(
                left: Get.width * 0.05, right: Get.width * 0.05),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Order your',
                    style: GoogleFonts.archivoBlack(
                      fontWeight: FontWeight.w500,
                      fontSize: 28,
                    )),
                Container(
                  margin: EdgeInsets.only(top: Get.height * 0.008),
                ),
                Text('Favourite food',
                    style: GoogleFonts.archivoBlack(
                        fontWeight: FontWeight.w500, fontSize: 28)),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: Get.height * 0.03),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    padding: EdgeInsets.only(
                        left: Get.width * 0.05, right: Get.width * 0.05),
                    margin: EdgeInsets.only(bottom: Get.height * 0.02),
                    child: const Text('Categories',
                        style: TextStyle(
                            fontSize: 22, fontWeight: FontWeight.w500))),
                SizedBox(
                    height: Get.height * 0.04,
                    child: ListView(
                      padding: EdgeInsets.only(
                          left: Get.width * 0.05, right: Get.width * 0.05),
                      scrollDirection: Axis.horizontal,
                      children: List.generate(controller.allCategories.length, (index) {
                        List<CategoryModel> categories = controller.allCategories;
                        return Container(
                          margin: EdgeInsets.only(right: Get.width * 0.03),
                          child: GestureDetector(
                            onTap: () {
                              controller.changeCategory(categories[index]);
                            },
                            child: CategoryWidget(category: categories[index], isSelected: categories[index].id == controller.selectedCategory.id,),
                          ),
                        );
                      }),
                    ))
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: Get.height * 0.03, bottom: Get.height * 0.03),
            height: Get.height * 0.07,
            child: ListView(
              padding: EdgeInsets.only(
                  left: Get.width * 0.05, right: Get.width * 0.05),
              scrollDirection: Axis.horizontal,
              children: List.generate(DummyData.favoriteFruits.length, (index) {
                ItemModel favItem = DummyData.favoriteFruits[index];
                return Container(
                  margin: EdgeInsets.only(right: Get.width * 0.05),
                  child: FavoriteItemWidget(item: favItem),
                );
              }),
            ),
          ),
          Container(
              padding: EdgeInsets.only(
                  left: Get.width * 0.05, right: Get.width * 0.05),
              margin: EdgeInsets.only(bottom: Get.height * 0.02),
              child: const Text('Popular drinks',
                  style: TextStyle(
                      fontSize: 22, fontWeight: FontWeight.w500))),
          Expanded(
            child: ListView(
              padding: EdgeInsets.only(top: 0, left: 0, right: 0, bottom: Get.height * 0.02),
              scrollDirection: Axis.vertical,
              children: List.generate(DummyData.favoriteFruits.length, (index) {
                ItemModel favItem = DummyData.favoriteDrinks[index];
                return GestureDetector(
                  onTap: () {
                    Get.toNamed(AppRoutes.ITEM_DETAILS);
                  },
                  child: ItemCardWidget(item: favItem),
                );
              }),
            ),
          ),
        ],
      );
    });
  }
}
