import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:the_yummy/data/dummy_data.dart';
import 'package:the_yummy/helper/color_helper.dart';
import 'package:the_yummy/widgets/item_counter_widget.dart';

class ItemDetailPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          padding: EdgeInsets.only(bottom: Get.height * 0.03),
          height: Get.height * 0.3,
          width: Get.width,
          decoration: BoxDecoration(color: ColorHelper.primary),
          child: Image.asset('assets/images/jeruk.png'),
        ),
        Container(
          margin: EdgeInsets.only(top: Get.height * 0.05),
          padding:
              EdgeInsets.only(left: Get.width * 0.03, right: Get.width * 0.03),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                decoration: BoxDecoration(
                    color: ColorHelper.dark,
                    borderRadius: BorderRadius.circular(5)),
                padding: EdgeInsets.all(1),
                child: Icon(Icons.navigate_before, color: ColorHelper.white),
              ),
              Image.asset(
                'assets/logos/man.png',
                width: Get.width * 0.1,
              )
            ],
          ),
        ),
        Positioned(
            child: Container(
          margin: EdgeInsets.only(top: Get.height * 0.25),
          padding: EdgeInsets.only(
              right: Get.width * 0.05,
              left: Get.width * 0.05,
              top: Get.height * 0.03),
          height: Get.height * 0.6,
          decoration: BoxDecoration(
              borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(30), topRight: Radius.circular(30)),
              color: ColorHelper.white),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text('Orange Juice',
                      style:
                          TextStyle(fontWeight: FontWeight.w600, fontSize: 25)),
                  Row(
                    children: [
                      Icon(Icons.star_rounded,
                          size: 25, color: ColorHelper.primary),
                      Icon(Icons.star_rounded,
                          size: 25, color: ColorHelper.primary),
                      Icon(Icons.star_rounded,
                          size: 25, color: ColorHelper.primary),
                      Icon(Icons.star_rounded,
                          size: 25, color: ColorHelper.primary),
                      Icon(Icons.star_rounded,
                          size: 25, color: ColorHelper.primary),
                    ],
                  )
                ],
              ),
              Container(
                margin: EdgeInsets.only(
                    top: Get.height * 0.03, bottom: Get.height * 0.03),
                child: Text(DummyData.copyText,
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        color: ColorHelper.darkGrey,
                        fontWeight: FontWeight.w600)),
              ),
              SizedBox(
                height: Get.height * 0.28,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                            child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text('Water',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        fontSize: 20)),
                                Text('3.50%',
                                    style: TextStyle(
                                        fontSize: 30,
                                        color: ColorHelper.primary,
                                        fontWeight: FontWeight.w600)),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text('Price',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        fontSize: 20)),
                                Text('\$5.20',
                                    style: TextStyle(
                                        fontSize: 30,
                                        color: ColorHelper.primary,
                                        fontWeight: FontWeight.w600)),
                              ],
                            ),
                          ],
                        )),
                        Container(
                          margin: EdgeInsets.only(
                              top: Get.height * 0.01,
                              bottom: Get.height * 0.03),
                          child: const ItemCounterWidget(),
                        )
                      ],
                    ),
                    Image.asset('assets/images/orange.png',
                        width: Get.width * 0.55,
                        fit: BoxFit.fitHeight,
                        height: Get.height * 0.28)
                  ],
                ),
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text('Reviews',
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w600)),
                        Text('See all',
                            style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                                color: ColorHelper.lightRed)),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ...List.generate(5, (index) {
                          return Image.asset(
                              'assets/images/avatar_${index + 1}.png',
                              width: Get.width * 0.13);
                        }),
                        DottedBorder(
                          borderType: BorderType.Circle,
                          color: ColorHelper.lightRed,
                          child: Container(
                            decoration:
                                const BoxDecoration(shape: BoxShape.circle),
                            padding: EdgeInsets.all(Get.width * 0.03),
                            child: Icon(
                              Icons.add,
                              color: ColorHelper.lightRed,
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ))
      ],
    );
  }
}
