import 'package:get/get.dart';
import 'package:the_yummy/controller/item_controller.dart';

class ItemBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ItemController>(() => ItemController());
  }

}
