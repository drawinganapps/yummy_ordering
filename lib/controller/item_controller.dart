import 'package:get/get.dart';
import 'package:the_yummy/data/dummy_data.dart';
import 'package:the_yummy/models/category_model.dart';

class ItemController extends GetxController {
  List<CategoryModel> _categories = [];
  CategoryModel _selectedCategory = DummyData.categories[0];

  List<CategoryModel> get allCategories => _categories;
  CategoryModel get selectedCategory => _selectedCategory;

  @override
  void onInit() {
    super.onInit();
    initData();
  }

  void initData() async {
    await Future.delayed(const Duration(seconds: 1), () {
      _categories = DummyData.categories;
    });
    update();
  }

  void changeCategory(CategoryModel category) {
    _selectedCategory = category;
    update();
  }
}
