import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:the_yummy/controller/item_bindings.dart';
import 'package:the_yummy/screens/dashboard_screen.dart';
import 'package:the_yummy/screens/item_detail_screen.dart';
import 'AppRoutes.dart';

class AppPages {
  static var list = [
    GetPage(
        name: AppRoutes.DASHBOARD,
        page: () => const DashboardScreen(),
        binding: ItemBindings()),
    GetPage(name: AppRoutes.ITEM_DETAILS, page: () => const ItemDetailScreen()),
  ];
}
