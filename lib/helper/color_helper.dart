import 'package:flutter/material.dart';

class ColorHelper {
  static Color white = const Color.fromRGBO(255,255,255, 1);
  static Color primary = const Color.fromRGBO(217, 171, 102, 1);
  static Color secondary = const Color.fromRGBO(245, 246, 253, 1);
  static Color tertiary = const Color.fromRGBO(246, 244, 240, 1);
  static Color grey = const Color.fromRGBO(230, 233, 239, 1);
  static Color darkGrey = const Color.fromRGBO(112, 111, 129, 1);
  static Color dark = const Color.fromRGBO(0, 0, 0, 1);
  static Color yellow = const Color.fromRGBO(255, 225, 116, 1);
  static Color darkArmy = const Color.fromRGBO(89, 119, 102, 1);
  static Color lightYellow = const Color.fromRGBO(254, 235, 210, 1);
  static Color lightPink = const Color.fromRGBO(255, 235, 235, 1);
  static Color red = const Color.fromRGBO(255, 50, 51, 1);
  static Color lightRed = const Color.fromRGBO(223, 71, 71, 1);
}
