import 'dart:ui';

class ItemModel {
  int id;
  String name;
  String icon;
  double price;
  Color bgColor;

  ItemModel(this.id, this.name, this.icon, this.price, this.bgColor);
}
